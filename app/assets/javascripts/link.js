// Atualiza o banco de dados quando marcar o check "Lido?"
let read = document.querySelectorAll('.read input');
read.forEach((item) => {
	item.onclick = function() {
		fetch('/links/check-read/' + this.parentNode.parentNode.id + '/' + this.checked)
		.then(function(response){
			response.json().then(function(data){
				location.reload();
			})
		})
		
	}
});

let linkUrl = document.querySelector("#link_url");
if(linkUrl != null){

	let timeout = null;

	linkUrl.onkeyup = function () { 
		fetch('/links/preview/' + this.value)
		.then(function(response){
			response.json()
			.then(function(data){
				if (data == null){
					data = {
						title : "",
						description : "",
						image : ""
					}
				}
				clearTimeout(timeout);

				timeout = setTimeout(function () {
					
					setTitle(data.title);
					setDescription(data.description);
					setImage(data.image);
				});
			})
		})
	}
	
}

function setImage(url){
	document.getElementById("preview-img").src = url;
}

function setTitle(title){
	document.getElementById("preview-title").innerHTML = title;
}

function setDescription(description){
	document.getElementById("preview-description").innerHTML = description;
}

