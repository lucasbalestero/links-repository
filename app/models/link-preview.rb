require 'open-uri'
class LinkPreview

    def initialize(url)
        @url = filter_url(url)
    end
    
    def metadata
        new_url = "https://" + @url
        begin
            @doc = Nokogiri::HTML(open(new_url))
            params = {
                image: get_image,
                description: get_description,
                title: get_title
            }
        rescue
            puts "Não foi possível realizar a conexão com o endereço #{@url}"
        end
    end

    def get_image
        content = get_content "meta[property='og:image']"
        if content.present?
            content.values[1]
        end
    end

    def get_description
        content = get_content "meta[property='description']"
        if content.present?
            content.values[1]
        else
            content = get_content "meta[property='og:description']"
            if content.present?
                content.values[1]
            end
        end
    end

    def get_title
        content = get_content "title"
        content.text
    end

    def get_content(selector)
        doc = @doc.search("head #{selector}")
        doc.first
    end

    def filter_url(url)
        url = url.sub(/https:(\/+)www\./, '') 
        url = url.sub(/http:(\/+)www\./, '')
        url = url.sub(/https:(\/+)/, '')
        url = url.sub(/http:(\/+)/, '')
        url
    end


end
