class Link < ApplicationRecord
  belongs_to :category

  def self.search(title)
    if(title)
      where('title LIKE ?', "%#{title}%").order("read ASC")
    else
      order("read ASC")
    end
  end

  def self.preview(url)
    require('link-preview')
    linkPreview = LinkPreview.new(url)
    linkPreview.metadata
  end

end
