module ApplicationHelper

    def readable_boolean(boolean)
        boolean ? 'Sim' : 'Não'
    end
end
