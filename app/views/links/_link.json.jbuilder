json.extract! link, :id, :title, :url, :read, :category_id, :created_at, :updated_at
json.url link_url(link, format: :json)
