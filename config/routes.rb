Rails.application.routes.draw do
  resources :links
  resources :categories

  root to: 'links#index'

  get 'links/index'

  get 'links/new'

  get 'links/create'

  get 'links/destroy'

  get 'links/edit'

  get 'links/update'

  get 'links/check-read/:id/:read', to: 'links#check_read'

  get 'links/preview/*url', to: 'links#preview'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
